import { Component, OnInit } from '@angular/core';
import { Users } from '../user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  user!:Users;
  panelOpenState = false;
  user1:Users[]=[];
  onClick: boolean=false;
  length:number=0;
  id:number=0;
  name:string="";
  phone:string="";
  Email:string="";
  address:string="";
  constructor() {
    this.user1=[{
      "id": 1,
      "name": "Uttham",
      "address":"Pune",
      "Phone": "9959790973",
      "Email": "u@gmail.com",
    },
    {
      "id": 2,
      "name": "Pulkit Bindal",
      "address": "Delhi",
      "Phone": "1234567890",
      "Email": "pbindal@gmail.com",
    },
    {
      "id": 3,
      "name": "Shubh Jain",
      "address": "Delhi",
      "Phone": "7246218922",
      "Email": "jainshubh@gmail.com",
    },
    {
      "id": 4,
      "name": "Parth Jain",
      "address": "Meerut",
      "Phone": "7246217811",
      "Email": "parthj@gmail.com",
    },
    {
      "id": 5,
      "name": "Sanchit Patel",
      "address": "Udaipur",
      "Phone": "9876218922",
      "Email": "patels@gmail.com",
    },
    {
      "id": 6,
      "name": "Happy Singh",
      "address": "Punjab",
      "Phone": "7266885577",
      "Email": "singh61@gmail.com",
    },
    
    {
        "id": 7,
        "name": "Ishan Kapoor",
        "address": "Mumbai",
        "Phone": "7246786922",
        "Email": "ishank87@gmail.com",
    },
    {
      "id": 8,
      "name": "John Wick",
      "address": "New York",
      "Phone": "7246718922",
      "Email": "wick69@gmail.com",
    },
    {
      "id": 9,
      "name": "Pritam Chauhan",
      "address": "Patna",
      "Phone": "7453218922",
      "Email": "pritam62@gmail.com",
    },
    {
      "id": 10,
      "name": "Paras Jain",
      "address": "Surat",
      "Phone": "7246254342",
      "Email": "paras98@gmail.com",
    }]
   
  }   
  delete(index: number) {
    this.user1.splice(index, 1);
    
  }
  onSubmit(){
    this.user={
      id:1,
      name:this.name,
      address:this.address,
      Phone:this.phone,
      Email:this.Email
    }
    console.log(this.user);
    // Process checkou t data here
    this.user1.push(this.user);
    alert("Saved successful");
    
  }

  ngOnInit(): void {
  }

}
